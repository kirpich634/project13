const createNode = (name, attributes, children) => {
    const node = document.createElement(name);

    Object.keys(attributes).forEach(attribute => node.setAttribute(attribute, attributes[attribute]));

    children.forEach(child => {
        if (child instanceof Node) {
            node.appendChild(child)
        } else {
            const { array, component } = child;

            array.forEach(e => node.appendChild(useComponent(e, component)));

            on(array, "set", (property, value, oldValue) => {
                if (property === "length")
                    return;

                const index = +property;

                if (oldValue) {
                    const oldChild = nodeMap.get(oldValue);

                    node.insertBefore(useComponent(value, component), oldChild);
                    node.removeChild(oldChild);
                } else {
                    const child = useComponent(value, component);

                    if (index === array.length - 1) {
                        let find = false;

                        for (let i = index; i > -1; i--) {
                            if (array[i]) {
                                find = true;

                                if (nodeMap.get(array[i]).nextSibling) {
                                    node.insertBefore(child, nodeMap.get(array[i]).nextSibling)
                                } else {
                                    node.appendChild(child)
                                }
                            }
                        }

                        if (!find) {

                            console.log("в конец");
                        }

                    } else {
                        console.log("в свободное место");

                        let find = false;
                        for (let i = index; i < array.length; i++) {
                            if (array[i]) {
                                find = true;
                                node.insertBefore(child, nodeMap.get(array[i]))
                            }
                        }

                        if (!find) {
                            console.log("в конец");
                        }
                    }
                }
            });
        }
    });

    return node;
};

const createTextNode = text => document.createTextNode(text);

const useComponent = (data, component) =>  {
    const node = component(data);

    if (nodeMap.has(data)) {
        nodeMap.get(data).push(node)
    } else {
        nodeMap.set(data, [node])
    }

    return node;
};

const bind = (data, param, node, nodeParam) => {
    on(data, "set", (property, value, oldValue) => {
        if (property !== param)
            return;

        node[nodeParam] = value;
    });

    return node;
};

const forBind = (array, component) => ({
    array: array,
    component: component
});

let requestAnimationFrameId = false;

const turn = [];

const executor = () => {
    turn.forEach((action, i) => {
        action();

        turn.splice(i, 1);
    });

    requestAnimationFrameId = false;
};

const make = action => {
    turn.push(action);

    if (!requestAnimationFrameId) {
        requestAnimationFrameId = requestAnimationFrame(executor);
    }
};

const nodeMap = new Map();

const eventsMap = new Map();

const trigger = function (data, name) {
    if (eventsMap.has(data)) {
        const events = eventsMap.get(data);

        if (events[name]) {
            events[name].forEach(callback => callback(...Array.prototype.slice.call(arguments, 2)));
        }
    }
};

const on = (target, name, callback) => {
    if (eventsMap.has(target)) {
        const events = eventsMap.get(target);

        if (events[name]) {
            events[name].push(callback);
        } else {
            events[name] = [
                callback
            ];
        }
    } else {
        eventsMap.set(target, {
            [name]: [
                callback
            ]
        })
    }
};

const off = (target, event, callback) => {
    if (eventsMap.has(target)) {
        const events = eventsMap.has(target);
        if (events[event]) {
            const index = events[event].indexOf(callback);
            if (index > -1) {
                events[event].splice(index, 1);
            }
        }
    }
};

const proxy = function (target) {
    Object.keys(target).forEach(key => {
        if (typeof target[key] === "object") {
            target[key] = proxy(target[key])
        }
    });

    const proxyTarget = new Proxy(target, {
        set(target, property, value, receiver) {

            if (typeof value === "object") {
                value = proxy(value)
            }

            const oldValue = target[property];

            target[property] = value;

            trigger(proxyTarget, "set", property, value, oldValue);

            return true;
        },
        deleteProperty(target, phrase) {
            console.log(arguments);

            trigger(proxyTarget, "delete", phrase);

            console.log(target, phrase);
            return true;
        }
    });

    return proxyTarget;
};

const data = proxy({
    heading: "Задачи на сегодня",
    tasks: [
        {
            heading: "задача 1"
        },
        {
            heading: "задача 2"
        }
    ]
});

const ti = data => {
    const item = createNode("div", {}, [
        bind(data, "heading", createTextNode(data.heading), "data")
    ]);

    return item;
};

const c1 = data => {
    const component = createNode("div", {}, [
        createNode("header", {}, [
            createTextNode(data.heading)
        ]),
        forBind(data.tasks, ti)
    ]);

    return component;
};

document.addEventListener("DOMContentLoaded", () => {
    if (document.body.children.length) {
        document.body.insertBefore(c1(data), document.body.children[0]);
    } else {
        document.body.appendChild(c1(data));
    }
});








